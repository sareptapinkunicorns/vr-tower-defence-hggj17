﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiSpawningScript : MonoBehaviour
{

	public GameObject[] spawnPoints;
	public GameObject theEnemy;

	// Use this for initialization
	void Start ()
	{
		spawnPoints = GameObject.FindGameObjectsWithTag ("Spawn");

	}
	
	// Update is called once per frame
	void Update ()
	{
		GameObject[] enemies;
		enemies = GameObject.FindGameObjectsWithTag ("Enemy");

		if (enemies.Length >= 4) {
			print ("Too many enemies");
		} else {
			InvokeRepeating ("spawnEnemies", 1, 5f);
		}

	}

	void spawnEnemies ()
	{
		int SpawnPos = Random.Range (0, (spawnPoints.Length - 0));

		GameObject enemy = Instantiate (theEnemy, spawnPoints [SpawnPos].transform.position, transform.rotation);
		enemy.name = theEnemy.name;
		CancelInvoke ();

	}
}
